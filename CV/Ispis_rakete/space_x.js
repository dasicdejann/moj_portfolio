"use strict";

let dugmee = document.getElementById("dugme");
dugmee.addEventListener("click", getData);
let povrat_podataka = document.getElementById("na_server");
povrat_podataka.addEventListener("click", vrati_na_server);
let tabela = document.querySelector('#teloTabele');
let pojedinacna_raketa;
let svojstva;
let vrednosti;


async function getData() {
    let response = await fetch("https://api.spacexdata.com/v4/capsules")
    let data = await response.json();
    pojedinacna_raketa = data[0];
    svojstva = Object.keys(pojedinacna_raketa);
    vrednosti = Object.values(pojedinacna_raketa);
    tabela.innerHTML = "";
    for (let podatak in svojstva) {
        tabela.innerHTML += `
        <tr>
    <td>${svojstva[podatak]}</td>
    <td>${vrednosti[podatak]}</td>
   </tr>`
    };
    return document.getElementById("na_server").style.display = "flex";
};

async function vrati_na_server() {
    console.log(pojedinacna_raketa)
    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify(pojedinacna_raketa)
    })
        .then((response) => response.json())

     document.getElementById("na_server").style.display="none";
     document.getElementById("tabela").style.display="none";
     alert("Uspešno ste vratili podatka, hvala na saradnji.");
};
