"use strict";

//U JS promenljive ubacićemo vrednosti, koje će korisnik uneti pri popunjavanju input-a.

let inflation_rate = document.querySelector("#inflationRate");
let money = document.querySelector("#money");
let years = document.querySelector("#years");
let container = document.querySelector(".container")
let calculator = document.querySelector("#button");
calculator.addEventListener("click", inflationCalculator);

//Sada kreiramo funkciju za izračunavanje inflacije, tj. vrednosti koju će valuta imati, 
//nakon proteka jedinice vremena:

function inflationCalculator() {
    let inflation_rate_value = Number(inflation_rate.value) / 100
    let inflation_value = Number(money.value) + (Number(money.value) * inflation_rate_value);
    for (let i = 1; i < Number(years.value); i++) {
        inflation_value += inflation_value * inflation_rate_value
    };
    let resolt = document.createElement("div");
    resolt.className = "new-value";
    resolt.innerText = `Sadašnjih ${money.value} 💲, za ${years.value} godina imaće ekvivalentnu vrednost od 
   ${inflation_value.toFixed(3)}, po nadevenoj stopi inflacije od ${inflation_rate.value} %`;
    container.appendChild(resolt);
};



