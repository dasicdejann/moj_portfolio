"use strict";

/*Zadatak 1
Prebrojati koliko ima reči u unetom stringu, tako što prebrojavamo razmake. 
Npr. za string 'Joca voli sladoled', dobija se rezultat 3. Podrazumeva se da u stringu nema vodećih, 
pratećih ili višestrukih razmaka.*/

function countWords(sentence) {
    if (sentence != "" && sentence != " ") {
        let space = 0;
        for (let i in sentence) {
            if (sentence[i] == " ") {
                space += 1
            };
        };
        return space += 1;
    }
    else { return "Uneta rečenica ne sadrži ni jednu reč." };
};

/*Zadatak 2
Prebrojati koliko se puta u unetom stringu pojavljuje slovo "M" (i veliko i malo). 
Npr. za string 'Mama ima momu', dobija se rezultat 5.*/

function countM(sentence) {
    //Napomena!
    //Zbog optimizacije brzine izvršavanja koda, metodu "toLowerCase" stavljamo izvan for petlje!"
    sentence = sentence.toLowerCase()
    let numberOfM = 0
    for (let i in sentence) {
        if (sentence[i] == "m") {
            numberOfM++
        }
    }
    return numberOfM;
};

//Isti zadatak, urađen pomoću metode ".filter"...

function count_some_thing(sentence, parameter) {
    sentence = sentence.toLowerCase();
    sentence = sentence.split("");
    let arraySentence = sentence.filter(x => x == parameter);
    return arraySentence.length
};

/*Zadatak 3
Prebrojati koliko ima znakova koji su cifre u unetom stringu.*/

function count_numbers(sentence) {
    sentence = sentence.split("");
    let arraySentence = sentence.filter(x => x >= "0" && x <= "9");
    return arraySentence.length;
};

/*Zadatak 4
Prebrojati koliko ima malih slova u unetom stringu. 
Slova koja se koriste u stringu pripadaju isključivo engleskoj abecedi.*/

function count_lowercase_letters(sentence) {
    sentence = sentence.split("");
    let arraySentence = sentence.filter(x => x >= "a" && x <= "z");
    return arraySentence.length;
};

/*Zadatak 5
Ispitati da li u unetom stringu ima više malih ili velikih slova. 
Slova koja se koriste u stringu pripadaju isključivo engleskoj abecedi*/

function more_lower_or_upper(sentence) {
    let newsentence = sentence.split("");
    let arrayUppercase = newsentence.filter(x => x >= "A" && x <= "Z");
    if (arrayUppercase.length > count_lowercase_letters(sentence)) {
        alert("U navedenom stringu ima više velikih slova")
    }
    else { alert("U navedenom stringu ima više malih slova") };
};

/*Zadatak 6
Proveriti da li je uneti string palindrom (potpuno je isti kada se čita od pozadi). 
Npr. 'anavolimilovana' je palindrom.*/
//Uzevši u obzir tekst narednog zadatka, predpostavićemo da u ovom slučaju, korisnik unosi rečenicu bez razmaka.

function check_palindrom(sentence) {
    sentence = sentence.toLowerCase();
    sentence = sentence.split("");
    let testArray = [];
    for (let i in sentence) {
        testArray.unshift(sentence[i])
    };
    if (JSON.stringify(sentence) == JSON.stringify(testArray)) {
        alert("Uneta rečenica je palindrom!")
    }
    else { alert("Uneta rečenica nije palindrom!") }
};

/*Zadatak 7
Proveriti da li je uneti string palindrom, vodeći računa o razmacima. 
Npr. 'ana voli milovana' je palindrom.*/

function check_full_palindrom(sentence) {
    let new_sentence = sentence.replaceAll(" ", "");
    return check_palindrom(new_sentence);
};

/*Zadatak 8
Sva velika slova u stringu treba svesti na mala slova. 
Npr. za uneto 'WEBnSTUDY.com', dobija se novi string 'webnstudy.com'. 
Slova koja se koriste u stringu pripadaju isključivo engleskoj abecedi.*/

function upper_to_lower(sentence) {
    return sentence.toLowerCase();
};

/*Zadatak 9
Pronaći poziciju poslednjeg razmaka u stringu, pretražujući od kraja stringa. 
Npr. za uneto 'Pera ima devojku', rezultat je 9 (pozicija prvog znaka je 1).*/

function finde_last_space(sentence) {
    if (sentence.includes(" ")) {
        let placeSpace;
        sentence = sentence.split("");
        for (let i in sentence) {
            if (sentence[i] === " ") {
                placeSpace = i
            }
        };
        return Number(placeSpace) + 1
    }
    else { alert("U unetoj rečenici ne postoje razmaci!") };
};

/*Zadatak 10
Ispisati dužinu prve reči unetog stringa. 
Npr. za uneto 'Pera ima devojku', rezultat je 4. 
Podrazumeva se da uneti string nema vodeće razmake.*/

function count_first_word(sentence) {
    sentence = sentence.split("");
    for (let i in sentence) {
        if (sentence[i] === " ")
            return i
    };
};

/*Zadatak 11
Za uneti string A treba kreirati novi string B koji se sastoji
 od prve i poslednje reči (odvojenih razmakom) unetog stringa.
  Npr. za A='Svuda pođi, kući dođi', dobija se B='Svuda dođi'. 
  Podrazumeva se da uneti string nema vodeće ili prateće razmake.*/

function cut_sentence(sentence) {
    sentence = sentence.split(" ");
    let finde_sentence = sentence.shift() + " " + sentence.pop();
    return finde_sentence;
};

//Evo i optimizovanijeg rešenja.

function cut_sentenceII(sentence) {
    sentence = sentence.split(" ");
    return sentence.shift() + " " + sentence.pop();
};

/*Zadatak 12
Iz unetog stringa treba izdvojiti N znakova sa leve strane.
Npr. za string 'Pera ima devojku' i N=6, dobija se 'Pera i'. 
Ako je N veće od dužine stringa, kao rezultat se dobija ceo string.*/

function get_first_chacackter(sentence, n) {
    if (n > sentence.length) {
        return sentence
    }
    else {
        sentence = sentence.substring(0, n);
        return sentence
    };
};

/*Zadatak 13
Izdvojiti N znakova sa desne strane (poslednjih N znakova) unetog stringa.
Npr. za string 'Pera ima devojku' i N=5, dobija se 'vojku'.*/


function get_last_string(sentence, n) {
    return sentence.substring(sentence.length - n, sentence.length);
}

/*Zadatak 14
Za uneti string A, treba izdvojiti podstring B, koji počinje od unete pozicije N i ima M znakova. 
Npr. za string 'Pera ima devojku', N=6 i M=7, dobija se B='ima dev'.*/

function get_sub_string(sentence, n, m) {
    return sentence.substring(--n, n + m);
};

/*Zadatak 15
Uneti string se šifruje tako što se zamenjuje redosled znacima u stringu. 
Prvi i drugi zamene mesta, zatim 3. i 4. zamene mesta itd. 
Npr. za string 'Pera ima devojku', treba dobiti 'ePari amd vejouk'. 
Ako string ima neparan broj znakova, poslednji znak se ne dira.*/

function switchPlaces(sentence) {
    let new_sentence = "";
    for (let i = 0; i < sentence.length; i++) {
        new_sentence += sentence[i + 1] + sentence[i]
    }
    if (sentence.length > new_sentence.length) {
        new_sentence += sentence[sentence.length]
    };
    console.log(new_sentence);
};

// Zadatak 16
// Unosi se string A i znak Z. 
// Kreirati novi string B koji se dobija tako što se iz unetog stringa izbacuje svaka pojava znaka Z. 
// Npr. za unet string "Madagaskar" i znak "a", dobijamo rezultat "Mdgskr"

function cut_some_string(sentence, string) {
    return sentence.toLowerCase().replaceAll(string.toLowerCase(), "")
};

// Zadatak 17
// Unose se dva stringa A i B.
// Kreirati novi string kao kombinaciju stringova A i B,
// tako što se kombinuju prvi sa prvim, drugi sa drugim, treći sa trećim znakom itd.
// Ako je jedan string duži od drugog, na kraju samo dodati znakove viška.
// Npr. za stringove "PERA" i "sladoled" dobija se string "PsElRaAdoled".

function string_combinater(sentence1, sentence2) {
    let final_string = "";
    for (let index1 in sentence1) {
        final_string += sentence1[index1]
        for (let index2 in sentence2) {
            if (index2 == index1) {
                final_string += sentence2[index2]
            };
        };
    };
    if (sentence1.lengt > sentence2.lengt) {
        final_string += sentence1.substring(sentence2.length, sentence1.length)
    }
    else if (sentence2.lengt > sentence1.lengt) {
        final_string += sentence2.substring(sentence1.length, sentence2.lengt)
    };
    return final_string;
};

/*Zadatak 18
Pronaći i ispisati najdužu reč u unetom stringu.
 Npr. za string "Joca voli sladoled od vanile", dobija se "sladoled".*/

function find_longest_word(sentence) {
    sentence = sentence.split(" ");
    let longest_word = ""
    for (let i in sentence) {
        if (sentence[i].length > longest_word.length) {
            longest_word = sentence[i]
        }
    };
    let new_sentence = sentence.filter(x => x.length == longest_word.length);
    return new_sentence.toString()
};



